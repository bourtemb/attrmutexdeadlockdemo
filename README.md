# AttrMutexDeadLockDemo device server

Device server which can be used to reproduce a deadlock which was first observed with ModbusComposer device server.
There are several situations where the deadlock can occur.
An AttrMutexDeadLockDemo Tango device is using a DeviceProxy on itself to read "value" attribute in its dev_state() method.
A deadlock occurs when a client is reading "value" attribute and "state" attribute in a read_attributes call.
The deadlock can occur as well when "value" and "state" attributes are polled at the same polling period (when using Tango >=9 default polling algorithm).

To compile the device server for your installation, please re-generate the Makefile or CMakeLists.txt with Pogo.
An example of client code doing the read_attributes call can be found in the client directory.
You will need to adapt the Makefile to your current installation in order to compile the TangoClient executable.
The TangoClient executable takes a device name as argin parameter. You should pass the name of the AttrMutexDeadLockDemo device you have created in your TANGO system as argin parameter.

