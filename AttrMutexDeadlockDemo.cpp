/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        AttrMutexDeadlockDemo.cpp
//
// description : C++ source for the AttrMutexDeadlockDemo class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               AttrMutexDeadlockDemo are implemented in this file.
//
// project :     AttrMutexDeadlockDemo
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2021
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <AttrMutexDeadlockDemo.h>
#include <AttrMutexDeadlockDemoClass.h>

/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo.cpp

/**
 *  AttrMutexDeadlockDemo class description:
 *    Simple class to reproduce a bug observed with the ModbusComposer class
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  dev_state
//  Status        |  Inherited (no method)
//  StopThrow     |  stop_throw
//  StartThrow    |  start_throw
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  value                 |  Tango::DevDouble	Scalar
//  ValueAttrSleepTimeMs  |  Tango::DevDouble	Scalar
//================================================================

namespace AttrMutexDeadlockDemo_ns
{
/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::AttrMutexDeadlockDemo()
 *	Description : Constructors for a Tango device
 *                implementing the classAttrMutexDeadlockDemo
 */
//--------------------------------------------------------
AttrMutexDeadlockDemo::AttrMutexDeadlockDemo(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::constructor_1
}
//--------------------------------------------------------
AttrMutexDeadlockDemo::AttrMutexDeadlockDemo(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::constructor_2
}
//--------------------------------------------------------
AttrMutexDeadlockDemo::AttrMutexDeadlockDemo(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::delete_device()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	delete myself;
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::delete_device
	delete[] attr_value_read;
	delete[] attr_ValueAttrSleepTimeMs_read;
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::init_device()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	
	attr_value_read = new Tango::DevDouble[1];
	attr_ValueAttrSleepTimeMs_read = new Tango::DevDouble[1];
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::init_device) ENABLED START -----*/
	
	//	Initialize device
	*attr_value_read = 0.0;
	myself = new Tango::DeviceProxy(device_name);
	*attr_ValueAttrSleepTimeMs_read = 1; // 1ms
	//myself->set_source(Tango::DEV);
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::init_device
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::get_device_property()
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("ValueAttrThrowAtStartup"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on AttrMutexDeadlockDemoClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		AttrMutexDeadlockDemoClass	*ds_class =
			(static_cast<AttrMutexDeadlockDemoClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize ValueAttrThrowAtStartup from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  valueAttrThrowAtStartup;
		else {
			//	Try to initialize ValueAttrThrowAtStartup from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  valueAttrThrowAtStartup;
		}
		//	And try to extract ValueAttrThrowAtStartup value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  valueAttrThrowAtStartup;

	}

	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::always_executed_hook()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::write_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::write_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute value related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::read_value(Tango::Attribute &attr)
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::read_value(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::read_value) ENABLED START -----*/
	if(valueAttrThrowAtStartup)
	{
		Tango::Except::throw_exception((const char *)"ForcedException",
		                               (const char *)"Exception while reading value attribute",
		                               (const char *)"AttrMutexDeadlockDemo::read_value");
	}
	*attr_value_read += 1.0;
	DEBUG_STREAM << "AttrMutexDeadlockDemo::read_value(): value = " << *attr_value_read << endl;
	//	Set the attribute value
	attr.set_value(attr_value_read);
	usleep(*attr_ValueAttrSleepTimeMs_read * 1000);
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::read_value
}
//--------------------------------------------------------
/**
 *	Read attribute ValueAttrSleepTimeMs related method
 *	Description: Sleep time in ms in read_value()
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::read_ValueAttrSleepTimeMs(Tango::Attribute &attr)
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::read_ValueAttrSleepTimeMs(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::read_ValueAttrSleepTimeMs) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_ValueAttrSleepTimeMs_read);
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::read_ValueAttrSleepTimeMs
}
//--------------------------------------------------------
/**
 *	Write attribute ValueAttrSleepTimeMs related method
 *	Description: Sleep time in ms in read_value()
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::write_ValueAttrSleepTimeMs(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::write_ValueAttrSleepTimeMs(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::write_ValueAttrSleepTimeMs) ENABLED START -----*/
	*attr_ValueAttrSleepTimeMs_read = w_val;
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::write_ValueAttrSleepTimeMs
}

//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command State related method
 *	Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
 *
 *	@returns Device state
 */
//--------------------------------------------------------
Tango::DevState AttrMutexDeadlockDemo::dev_state()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::State()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::dev_state) ENABLED START -----*/
	
	Tango::DevState	argout = Tango::ON;
	try
	{
			Tango::DeviceAttribute da = myself->read_attribute("value");
			Tango::DevDouble val = 0.0;
			da >> val;
			DEBUG_STREAM << "AttrMutexDeadlockDemo::dev_state(): value = " << val << endl;
	}
	catch(Tango::DevFailed &e)
	{
		Tango::Except::print_exception(e);
	}
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::dev_state
	set_state(argout);    // Give the state to Tango.
	if (argout!=Tango::ALARM)
		Tango::DeviceImpl::dev_state();
	return get_state();  // Return it after Tango management.
}
//--------------------------------------------------------
/**
 *	Command StopThrow related method
 *	Description: After this command is executed, the value attribute does not throw 
 *               exceptions any longer.
 *
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::stop_throw()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::StopThrow()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::stop_throw) ENABLED START -----*/
	
	//	Add your own code
	valueAttrThrowAtStartup = false;
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::stop_throw
}
//--------------------------------------------------------
/**
 *	Command StartThrow related method
 *	Description: After this command has been executed, the value attribute throws  exceptions
 *
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::start_throw()
{
	DEBUG_STREAM << "AttrMutexDeadlockDemo::StartThrow()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::start_throw) ENABLED START -----*/
	
	//	Add your own code
	valueAttrThrowAtStartup = true;
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::start_throw
}
//--------------------------------------------------------
/**
 *	Method      : AttrMutexDeadlockDemo::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void AttrMutexDeadlockDemo::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::add_dynamic_commands
}

/*----- PROTECTED REGION ID(AttrMutexDeadlockDemo::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	AttrMutexDeadlockDemo::namespace_ending
} //	namespace
