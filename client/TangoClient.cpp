#include <tango.h>
#include <chrono>

using namespace Tango;
using namespace std;




int main(int argc, char ** argv)
{
	string device_name = "test/rb/att_mut_deadlock";
	if(argc >= 2)
	{
		device_name = argv[1];
	}


	try
	{
		Tango::DeviceProxy proxy(device_name);
		proxy.set_source(Tango::DEV);
		vector<string> attr_names;
 		
		attr_names.push_back("value");
		attr_names.push_back("state");
 		
		vector<Tango::DeviceAttribute> *devattr = nullptr;
		devattr = proxy.read_attributes(attr_names);
 		
		Tango::DevState the_state = Tango::UNKNOWN;
		Tango::DevDouble the_value = -44;
		
		try
		{
			(*devattr)[1] >> the_state;
			cout << device_name << " state = " << the_state << endl;
	
		}
		catch(const Tango::DevFailed &e)
		{
			Tango::Except::print_exception(e);
		}
		try
		{
			(*devattr)[0] >> the_value;
			cout << device_name << " value  = " << the_value << endl;
		}
		catch(const Tango::DevFailed &e)
		{
			Tango::Except::print_exception(e);
		}
	}
	catch(Tango::DevFailed &e)
	{
		Tango::Except::print_exception(e);
	}
		
	return 0;
}
